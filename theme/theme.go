package theme

import (
	"image/color"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/theme"
)

type CustomTheme struct{}

var _ fyne.Theme = (*CustomTheme)(nil)

var defaultTheme = theme.LightTheme()

// return bundled font resource
func (*CustomTheme) Font(s fyne.TextStyle) fyne.Resource {
	if s.Monospace {
		return defaultTheme.Font(s)
	}
	if s.Bold {
		if s.Italic {
			return defaultTheme.Font(s)
		}
		return fontBoldResource
	}
	if s.Italic {
		return defaultTheme.Font(s)
	}
	return fontRegularResource
}

func (*CustomTheme) Color(n fyne.ThemeColorName, v fyne.ThemeVariant) color.Color {
	return defaultTheme.Color(n, v)
}

func (*CustomTheme) Icon(n fyne.ThemeIconName) fyne.Resource {
	return defaultTheme.Icon(n)
}

func (*CustomTheme) Size(n fyne.ThemeSizeName) float32 {
	return defaultTheme.Size(n)
}
